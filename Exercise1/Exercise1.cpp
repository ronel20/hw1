#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>
#include <math.h>

#define BINARY_NUM_CHAR_ARRAY_MAX_SIZE_UNSIGNED 32
#define INT_INITIALIZATION 0

int charArrayToInt(char[]);
int highestPowOfTwo(unsigned int);
void decimalToBinary(unsigned int,char[], unsigned int);

int main()
{
	unsigned int num;
	scanf("%d", &num);
	const int maxPow = highestPowOfTwo(num);
	char binaryValue[BINARY_NUM_CHAR_ARRAY_MAX_SIZE_UNSIGNED];
	decimalToBinary(num, binaryValue, maxPow);
	printf("binary value of %d is %s", num , binaryValue);
}

int charArrayToInt(char str[])
{
	unsigned int num = INT_INITIALIZATION;
	unsigned int multiplyer = 1;
	for (int i = strlen(str) - 1 ; i >= 0; i-- ,multiplyer*=10)
	{
		num += (int)(str[i] - '0') * multiplyer;
	}
	return num;	
}

int highestPowOfTwo(unsigned int num)
{
	unsigned int power = INT_INITIALIZATION;
	while (pow(2, power) < num)
	{
		power += 1;
	}
	if (pow((float)2, power) == num)
	{
		return power;
	}
	else
	{
		return power - 1;
	}
}

void decimalToBinary(unsigned int num,char binaryNum[],unsigned int maxPow)
{
	unsigned int i;
	for (i = INT_INITIALIZATION; i <= maxPow; i++)
	{
		if (num / pow(2,(maxPow-i)) >= 1)
		{
			binaryNum[i] = '1';
			num = num % (int)pow((float)2, (float)(maxPow-i));
		}
		else
		{
			binaryNum[i] = '0';
		}
	}
	binaryNum[i] = '\0';
}

