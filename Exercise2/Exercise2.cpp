
#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

#define MAX_STRING_LENGTH 128

void printFromIndex(char[], int);
int str_scan(char[], char);

int main()
{
	char string[MAX_STRING_LENGTH+1];
	char charecter;
	printf("enter a string:");
	gets_s(string);
	printf("enter a charecter:");
	charecter = getchar();
	return (str_scan(string, charecter));
	
}

void printFromIndex(char str[], int index) 
{
	while (str[index] != '\0')
	{
		printf("%c", str[index]);
		index++;
	}
}

int str_scan(char str[], char ch)
{
	int i = 0;
	int count = 0;
	while (str[i] != '\0')
	{
		if (str[i] == ch)
		{
			printFromIndex(str, i);
			printf("\n");
			count++;
		}
		i++;
	} 
	return count;
}

